import pandas as pd
import time
import datetime

def readTsvToDf(path):
    return pd.read_csv(path,sep='\t')

log_exec = list()
log_exec.append("\n ---------Execução Pandas Iniciando em {} ----------\n".format(datetime.datetime.now()))

ini = time.time()
df = readTsvToDf('./datasources/title.principals.tsv.gz')
fim = time.time()


log_exec.append("Tempo de leitura {} segs \n".format((fim - ini)))

ini = time.time()
print(df.groupby('category').count())
fim = time.time()

log_exec.append("Tempo de processamento de contagem por categoria {} segs\n".format((fim - ini)))

ini = time.time()
df.to_parquet('./output/title.principals.pandas.parquet')
fim = time.time()

log_exec.append("Tempo de conversão em arquivo parquet {} segs\n".format((fim - ini)))

log_file = open('./log.txt','a')
log_file.writelines(log_exec)