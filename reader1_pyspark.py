import pyspark 
import time
import datetime


spark =  (
    pyspark.sql
    .SparkSession
    .builder
    .getOrCreate()
)

spark.sparkContext.setLogLevel('WARN')
log_exec = list()
log_exec.append("\n ---------Execução PySpark Iniciando em {} ----------\n".format(datetime.datetime.now()))



path = './datasources/title.principals.tsv.gz'
ini = time.time()
df = spark.read.csv(path, sep=r'\t', header=True)
fim = time.time()

log_exec.append("Tempo de leitura {} segs \n".format((fim - ini)))

ini = time.time()
df.groupBy('category').count().show()
fim = time.time()
log_exec.append("Tempo de operação contagem por grupo {} segs \n".format((fim - ini)))

ini = time.time()
df.write.parquet("./output/title.principals.pyspark.parquet")
fim = time.time()
log_exec.append("Tempo de conversão em arquivo parquet {} segs\n".format((fim - ini)))

log_file = open('./log.txt','a')
log_file.writelines(log_exec)